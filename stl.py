import glob
import os
import sys
import subprocess

gccbin = subprocess.getoutput ('which gcc')
gccdir = os.path.dirname(os.path.dirname(gccbin))
gccpydir = glob.glob(gccdir + '/share/*/python')[0]
sys.path.append (gccpydir)

from libstdcxx.v6.printers import register_libstdcxx_printers
register_libstdcxx_printers(None)
