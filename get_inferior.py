import gdb

def get_inferior():
    """Return the PID of the current inferior, or None."""
    inf = gdb.selected_inferior()
    if inf is None: return None
    return inf.pid

