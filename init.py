# Munge sys.path to something usable.
# When we're started from the Atlas environment, we'll have everything
# that Atlas sets up in our path.  This stuff will in general not work
# with the python version built into gdb.  So clean everything off
# of the path except for system directories.  Then add to the path
# our current directory.
# Preserve the first path in the list --- that's the gdb data directory.

import sys
import os
import gdb
def _fdummy(): return
thisdir = os.path.dirname (_fdummy.__code__.co_filename)
sys.path = ([sys.path[0], os.path.expanduser(thisdir)] +
            [p for p in sys.path[1:] if
            p.startswith('/usr/lib') or p.find('external/Python')>=0 or
             (p.find('/LCG')>=0 and p.find('/Python/')>=0)])

# Put this directory the source path, for `source'.
gdb.execute ("dir %s" % thisdir)

import findlib
import sources
import btload
import pahole
import offsets
gdb.execute ("source importcmd")

